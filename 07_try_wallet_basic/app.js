var CryptoJS = require("crypto-js");
var Accounts = require('ethereumjs-account');






function generateSingleWallet(password) {
  // reference from staticJS/01_ethereumjs-accounts.js
  var acc = new Accounts();
  var newAccountEnc = acc.new(password);
  var addressHash =
    cryptoJSToHex(CryptoJS.SHA3(newAccountEnc.address));
  addressHash = addressHash.substr(addressHash.length - 4);

  var newAccountUnEnc = acc.get(newAccountEnc.address, password);
  // now: newAccountUnEnc.private is unencoded private key
  newAccountEnc.private = newAccountEnc.private + addressHash;
  // now: newAccountEnc.private is encoded private key

  // always clear the Accounts object after generation
  acc.clear();
}

console.log(generateSingleWallet("noushad"));
