var ethers = require('ethers');
var web3 = require('web3');

var provider = ethers.providers.getDefaultProvider('rinkeby');

var transaction = require('./transaction');

var config = require('./config');


var privateKey = '0x3ab6468f2465130c51946a5456b8e2d309be7af2f8afcd6823996d281c0990d0';



//Patient 1

var contract = new ethers.Contract(config.contract_address,config.abi,provider);

  var patientaddress = "0xF3DacA5383245AcedF64963B271dfF6dC116F883";
  var doctoraddress = "0x7c477A59578710eC7bfD2bf29D7a24F53A33979a";



  function ownership(patientaddress,doctoraddress) {
      var data = {
          type:'grant'
      }
      if(data.type=='grant'){
		  transaction.getDoctorAccessList(doctoraddress)
		  .then(doctorAccessList => {
			  console.log(doctorAccessList);
			  var d = doctorAccessList.indexOf(patientaddress);
			  console.log(d);
			  transaction.getPatientSharedList(patientaddress)
			  .then(sharedList => {
				  console.log(sharedList);
				  var p = sharedList.indexOf(doctoraddress);
				  console.log(p);
					  if(p==-1 && d==-1){
						  transaction.giveOwnership(patientaddress,doctoraddress,'0x3ab6468f2465130c51946a5456b8e2d309be7af2f8afcd6823996d281c0990d0')
						  .then(ownershipTransaction=> {
							console.log(ownershipTransaction);
							transaction.provider.waitForTransaction(ownershipTransaction.hash)
							.then(transactionComplete => {
										  console.log("Ownership Granted");
									  })
							})
				  }else{
					  console.log("Patient has already given access to this Doctor");
				  }
			  })	  
		  })
      }else if(data.type=='cancel'){
		  //code here for cancel access
		  transaction.getDoctorAccessList(doctoraddress)
		  .then(getdoctorlist =>{
			  console.log(getdoctorlist);
			var d = getdoctorlist.indexOf(patientaddress);
				  console.log(d);
				  transaction.getPatientSharedList(patientaddress)
				  .then(getSharedList => {
					  console.log(getSharedList);
					  var p = getSharedList.indexOf(doctoraddress);
					  console.log(p);
					  if(p!==-1 && d!==-1){
						transaction.cancelOwnership(patientaddress,p,doctoraddress,d,'0x3ab6468f2465130c51946a5456b8e2d309be7af2f8afcd6823996d281c0990d0')
						.then(cancelOwnershipTransaction => {
							console.log(cancelOwnershipTransaction);
							transaction.provider.waitForTransaction(cancelOwnershipTransaction.hash)
							.then(transactionComplete=> {
								console.log("Ownership Cancelled");
							})

						})
					         }else{
                              console.log("Doctor does not have access to cancel");
							 }
					 

					  })
				  })
		  }

	  }



//   ownership(patientaddress,doctoraddress);


  transaction.getDoctorAccessList(doctoraddress)
		  .then(doctorAccessList => {
			  console.log("docotrlist",doctorAccessList);
			  console.log(typeof(doctorAccessList));
			  if (doctorAccessList.includes('0x0000000000000000000000000000000000000000')){
				  console.log("has zero 0x00");
              var myArray = new Set(doctorAccessList);
			  console.log(myArray);
			  myArray.delete('0x0000000000000000000000000000000000000000');
			  console.log(myArray);
			  }else{
				var myArray = new Set(doctorAccessList);
				console.log(myArray);
			  }
			  
			  

		  });



  


