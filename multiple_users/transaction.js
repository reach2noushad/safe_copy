var ethers = require('ethers');
var config = require('./config');
var web3 = require('web3');

function instantiateWallet (privateKey) {
    var wallet = new ethers.Wallet(privateKey);
    wallet.provider = ethers.providers.getDefaultProvider('rinkeby');
    return wallet;
}

var provider  = ethers.providers.getDefaultProvider('rinkeby');

module.exports = {
    provider : ethers.providers.getDefaultProvider('rinkeby'),

    addEthersToUserAccount : function (admin_sk, user_pk) {
        console.log("add ethers transaction")
        const amount = ethers.utils.parseEther('0.5');
        const wallet = instantiateWallet(admin_sk);
        return wallet.send(user_pk, amount);
    },

    insertUser : function (publicKey, privateKey, openmrsID, data) {

        console.log("Insert User to Blockchain", openmrsID, web3.utils.fromAscii(openmrsID));
        const wallet = instantiateWallet(privateKey);
        const contract = new ethers.Contract(config.contract_address, config.abi, wallet);
        console.log(data);
        if(data.user === "Doctor") {
            console.log(web3.utils.fromAscii(data.InputAdhaar), web3.utils.fromAscii(openmrsID), data.InputOutputIdentifier, web3.utils.fromAscii(data.Department), web3.utils.fromAscii(data.Hospital));
            return contract.insertDoctor(publicKey, web3.utils.fromAscii(data.InputAdhaar), web3.utils.fromAscii(openmrsID), data.InputOutputIdentifier, web3.utils.fromAscii(data.Department), web3.utils.fromAscii(data.Hospital));
        }
        else if(data.user === "Patient"){
            console.log("patient sign up");
            console.log(publicKey, web3.utils.fromAscii(data.InputAdhaar), web3.utils.fromAscii(openmrsID), data.InputOutputIdentifier);
            return contract.insertPatient(publicKey, web3.utils.fromAscii(data.InputAdhaar), web3.utils.fromAscii(openmrsID), data.InputOutputIdentifier);
        } 
    },

    getDoctorAccessList : function(publicKey) {

        console.log("Getting the Details of Patient Addresses accessible to Doctor");
        const contract = new ethers.Contract(config.contract_address, config.abi, provider);
        return contract.getGivenAccesslist(publicKey);

    },

    getPatientSharedList:function(publicKey){

        console.log("Getting the Details of Doctors whom the patient has shared the data with");
        const contract = new ethers.Contract(config.contract_address, config.abi, provider);
        return contract.getPatientSharedList(publicKey);

    },

    giveOwnership : function(patientPubKey,doctorPubKey,privateKey){

        console.log("Patient Giving Access To Doctor");
        const wallet = instantiateWallet(privateKey);
        const contract = new ethers.Contract(config.contract_address, config.abi, wallet);
        return contract.secondOwner(patientPubKey,doctorPubKey);

    },

    addPatientFile : function(patientPubKey,url,privateKey) {

        console.log("Patient Adding Dicom File");
        const wallet = instantiateWallet(privateKey);
        const contract = new ethers.Contract(config.contract_address, config.abi, wallet);
        return contract.addhashpatient(patientPubKey,url);

    },

    cancelOwnership : function(patientPubKey,p,doctorPubKey,d,privateKey) {

        console.log("Patient Cancelling Accessing Given To Doctor");
        const wallet = instantiateWallet(privateKey);
        const contract = new ethers.Contract(config.contract_address, config.abi, wallet);
        return contract.cancelsecondowner(patientPubKey,p,d,doctorPubKey);

    },

    getPatientFromDoctor : function(doctorPubKey,patient_index) {

        console.log("Doctor Accessing Patient Data");
        const contract = new ethers.Contract(config.contract_address, config.abi, provider);
        return contract.getPatient(doctorPubKey,patient_index);
        
    },

    getPatientFileFromDoctor : function(doctorPubKey,patient_index) {

        console.log("Doctor Accessing Patient Dicom File");
        const contract = new ethers.Contract(config.contract_address, config.abi, provider);
        return contract.getpatienthashdoctor(doctorPubKey,patient_index);

    }
}