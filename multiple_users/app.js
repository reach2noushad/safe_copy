var ethers = require('ethers');
var web3 = require('web3');

var provider = ethers.providers.getDefaultProvider('rinkeby');



var config = require('./config');

var privateKey = '0x3ab6468f2465130c51946a5456b8e2d309be7af2f8afcd6823996d281c0990d0';

var wallet = new ethers.Wallet(privateKey,provider);

var contract = new ethers.Contract(config.contract_address,config.abi,wallet);

var addr = '0x7c477A59578710eC7bfD2bf29D7a24F53A33979a';
var adhaar = '121212121212';
var uuid = 'a288068d9ca24641a00e851d346608d7';
var comb = 'da39a3ee5e6b4b0d3255bfef95601890afd80709';


var sendPromise = contract.insertDoctor(addr,web3.utils.fromAscii(adhaar),web3.utils.fromAscii(uuid),comb,web3.utils.fromAscii('oncology'),web3.utils.fromAscii('MIMS'));

sendPromise.then(function(transaction){
  console.log(transaction);
  console.log(transaction.hash);
  provider.waitForTransaction(transaction.hash).then(function(result){
      console.log("Transaction Mined");
  });
})