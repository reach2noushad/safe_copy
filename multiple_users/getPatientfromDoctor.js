var ethers = require('ethers');
var web3 = require('web3');

var provider = ethers.providers.getDefaultProvider('rinkeby');

var transaction = require('./transaction');



var config = require('./config');

var privateKey = '0x3ab6468f2465130c51946a5456b8e2d309be7af2f8afcd6823996d281c0990d0';

var wallet = new ethers.Wallet(privateKey,provider);

var contract = new ethers.Contract(config.contract_address,config.abi,wallet);

var doctoraddress = "0x7c477A59578710eC7bfD2bf29D7a24F53A33979a";
var patientaddress = "0xF3DacA5383245AcedF64963B271dfF6dC116F883";

function getPatientfromDoctor(doctoraddress,patientaddress) {
    transaction.getDoctorAccessList(doctoraddress)
    .then(doctorAccessList => {
        console.log(doctorAccessList);
        if(doctorAccessList.includes(patientaddress)){
            console.log("Patient Present in Doctor's Access List");
            var patient_at_index = doctorAccessList.indexOf(patientaddress);
            console.log(patient_at_index);
            transaction.getPatientFromDoctor(doctoraddress,patient_at_index)
            .then(callValue => {
                console.log(callValue);
            })
        }else {
            console.log("Patient Not Present in the Doctors List");
        }
    })

}

getPatientfromDoctor(doctoraddress,patientaddress);


function getPatientFileFromDoctor(doctoraddress,patientaddress) {
    transaction.getDoctorAccessList(doctoraddress)
    .then(doctorAccessList => {
        console.log(doctorAccessList);
        if(doctorAccessList.includes(patientaddress)){
            console.log("Patient Present in Doctor's Access List");
            var patient_at_index = doctorAccessList.indexOf(patientaddress);
            console.log(patient_at_index);
            transaction.getPatientFileFromDoctor(doctoraddress,patient_at_index)
            .then(callValue => {
                console.log(callValue);
            })
        }else {
            console.log("Patient Not Present in the Doctors List");
        }
    })

}

// getPatientFileFromDoctor(doctoraddress,patientaddress)

// transaction.addPatientFile(patientaddress,'https://firebasestorage.googleapis.com/v0/b/noushad-18920.appspot.com/o/images%2Fbbmri-53323851.dcm?alt=media&token=df402da4-8938-427d-b21a-73558289eaaa','0x3ab6468f2465130c51946a5456b8e2d309be7af2f8afcd6823996d281c0990d0')
// .then(result => {
//     console.log(result);
// })