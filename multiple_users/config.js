exports.secretKey = "1234567890abcdefghijklmnopqrstuv";
exports.contract_address = '0xf7b85196489f5b5c665a07a436eed88ea303fc4a';
exports.admin_pk = '0x1bb4a50289de3c095afe5f40fa0ebb105aba0e917f3e4b36a74d847c683232da';
exports.cipheKeyIv = { 
  iv: Buffer.from("1234567890abcdefghijklmnopqrstuv").toString('hex').slice(0, 16),
  key: `1234567890abcdefghijklmnopqrstuv` 
}
exports.APP = "1b671a64-40d5-491e-99b0-da01ff1f3341";
exports.abi = [
	{
		"constant": false,
		"inputs": [
			{
				"name": "instowner",
				"type": "address"
			},
			{
				"name": "hash",
				"type": "string"
			}
		],
		"name": "addhashpatient",
		"outputs": [
			{
				"name": "success",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "instowner",
				"type": "address"
			},
			{
				"name": "UUIDs",
				"type": "bytes32"
			},
			{
				"name": "aadhar",
				"type": "bytes32"
			}
		],
		"name": "loginpatient",
		"outputs": [
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "instowner",
				"type": "address"
			},
			{
				"name": "UUIDs",
				"type": "bytes32"
			}
		],
		"name": "gethashpatient",
		"outputs": [
			{
				"name": "hash",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "patientaddress",
				"type": "address"
			},
			{
				"name": "doctoraddress",
				"type": "address"
			}
		],
		"name": "secondOwner",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "Doctor",
				"type": "address"
			},
			{
				"name": "d",
				"type": "uint256"
			}
		],
		"name": "getpatienthashdoctor",
		"outputs": [
			{
				"name": "hash",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "doctor",
				"type": "address"
			}
		],
		"name": "getGivenAccesslist",
		"outputs": [
			{
				"name": "",
				"type": "address[]"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "instowner",
				"type": "address"
			},
			{
				"name": "adhaar",
				"type": "bytes32"
			},
			{
				"name": "UUID",
				"type": "bytes32"
			},
			{
				"name": "comb",
				"type": "string"
			}
		],
		"name": "insertPatient",
		"outputs": [
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "instowner",
				"type": "address"
			},
			{
				"name": "adhaar",
				"type": "bytes32"
			},
			{
				"name": "UUID",
				"type": "bytes32"
			},
			{
				"name": "comb",
				"type": "string"
			},
			{
				"name": "department",
				"type": "bytes32"
			},
			{
				"name": "hospital",
				"type": "bytes32"
			}
		],
		"name": "insertDoctor",
		"outputs": [
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "patientowner",
				"type": "address"
			},
			{
				"name": "p",
				"type": "uint256"
			},
			{
				"name": "d",
				"type": "uint256"
			},
			{
				"name": "secondowner",
				"type": "address"
			}
		],
		"name": "cancelsecondowner",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "Doctor",
				"type": "address"
			},
			{
				"name": "d",
				"type": "uint256"
			}
		],
		"name": "getPatient",
		"outputs": [
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "patient",
				"type": "address"
			}
		],
		"name": "getPatientSharedList",
		"outputs": [
			{
				"name": "",
				"type": "address[]"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "instowner",
				"type": "address"
			},
			{
				"name": "UUIDs",
				"type": "bytes32"
			},
			{
				"name": "aadhar",
				"type": "bytes32"
			}
		],
		"name": "loginDoctor",
		"outputs": [
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "string"
			},
			{
				"name": "",
				"type": "bytes32"
			},
			{
				"name": "",
				"type": "bytes32"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	}
];