var ethers = require('ethers');

var providers = ethers.providers;

var abi = [{"constant":false,"inputs":[{"name":"newname","type":"string"}],"name":"addname","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"getname","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"}];

// console.log(providers);

var provider = providers.getDefaultProvider('rinkeby');

var address = '0x3450811b2832a4117cec25db1bc7ace48db29e05';

var contract = new ethers.Contract(address,abi,provider);

var callPromise = contract.getname();

callPromise.then(function(result){
  console.log(result);
})
