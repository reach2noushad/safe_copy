var ethers = require('ethers');

var privateKey = '0x7dcd3fcd70f6df14b46749ae0fd491048d9c0743c3e20f59bb0363554a02caaf';

var wallet = new ethers.Wallet(privateKey);

var provider = ethers.providers.getDefaultProvider('rinkeby');

wallet.provider = ethers.providers.getDefaultProvider('rinkeby');

// We must pass in the amount as wei (1 ether = 1e18 wei), so we use
// this convenience function to convert ether to wei.
var amount = ethers.utils.parseEther('1.0');

var address = '0xF3DacA5383245AcedF64963B271dfF6dC116F883';
var sendPromise = wallet.send(address, amount);

sendPromise.then(function(transactionHash) {
    console.log(transactionHash.hash);
    provider.waitForTransaction(transactionHash.hash).then(function(transaction) {
    console.log('Transaction Mined: ' + transaction.hash);
    console.log(transaction);
   });
});
