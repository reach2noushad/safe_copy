response = {
    "data": {
        "request": [
            {
                "type": "City",
                "query": "London, United Kingdom"
            }
        ],
        "current_condition": [
            {
                "observation_time": "10:56 AM",
                "temp_C": "10",
                "temp_F": "50",
                "weatherCode": "143",
                "weatherIconUrl": [
                    {
                        "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0006_mist.png"
                    }
                ],
                "weatherDesc": [
                    {
                        "value": "Haze"
                    }
                ],
                "windspeedMiles": "9",
                "windspeedKmph": "15",
                "winddirDegree": "90",
                "winddir16Point": "E",
                "precipMM": "0.6",
                "humidity": "87",
                "visibility": "3",
                "pressure": "1001",
                "cloudcover": "100",
                "FeelsLikeC": "8",
                "FeelsLikeF": "46"
            }
        ],
        "weather": [
            {
                "date": "2018-04-10",
                "astronomy": [
                    {
                        "sunrise": "06:16 AM",
                        "sunset": "07:49 PM",
                        "moonrise": "04:11 AM",
                        "moonset": "01:26 PM",
                        "moon_phase": "Waning Crescent",
                        "moon_illumination": "41"
                    }
                ],
                "maxtempC": "15",
                "maxtempF": "60",
                "mintempC": "9",
                "mintempF": "49",
                "totalSnow_cm": "0.0",
                "sunHour": "6.7",
                "uvIndex": "2",
                "hourly": [
                    {
                        "time": "0",
                        "tempC": "9",
                        "tempF": "49",
                        "windspeedMiles": "5",
                        "windspeedKmph": "9",
                        "winddirDegree": "84",
                        "winddir16Point": "E",
                        "weatherCode": "302",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0034_cloudy_with_heavy_rain_night.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Moderate rain"
                            }
                        ],
                        "precipMM": "3.1",
                        "humidity": "91",
                        "visibility": "7",
                        "pressure": "999",
                        "cloudcover": "100",
                        "HeatIndexC": "9",
                        "HeatIndexF": "49",
                        "DewPointC": "8",
                        "DewPointF": "46",
                        "WindChillC": "8",
                        "WindChillF": "46",
                        "WindGustMiles": "7",
                        "WindGustKmph": "12",
                        "FeelsLikeC": "8",
                        "FeelsLikeF": "46",
                        "chanceofrain": "98",
                        "chanceofremdry": "0",
                        "chanceofwindy": "0",
                        "chanceofovercast": "88",
                        "chanceofsunshine": "0",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    },
                    {
                        "time": "300",
                        "tempC": "9",
                        "tempF": "48",
                        "windspeedMiles": "4",
                        "windspeedKmph": "7",
                        "winddirDegree": "60",
                        "winddir16Point": "ENE",
                        "weatherCode": "296",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0033_cloudy_with_light_rain_night.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Light rain"
                            }
                        ],
                        "precipMM": "1.8",
                        "humidity": "92",
                        "visibility": "7",
                        "pressure": "997",
                        "cloudcover": "89",
                        "HeatIndexC": "9",
                        "HeatIndexF": "48",
                        "DewPointC": "8",
                        "DewPointF": "46",
                        "WindChillC": "8",
                        "WindChillF": "47",
                        "WindGustMiles": "6",
                        "WindGustKmph": "9",
                        "FeelsLikeC": "8",
                        "FeelsLikeF": "47",
                        "chanceofrain": "97",
                        "chanceofremdry": "0",
                        "chanceofwindy": "0",
                        "chanceofovercast": "82",
                        "chanceofsunshine": "0",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    },
                    {
                        "time": "600",
                        "tempC": "9",
                        "tempF": "49",
                        "windspeedMiles": "4",
                        "windspeedKmph": "6",
                        "winddirDegree": "104",
                        "winddir16Point": "ESE",
                        "weatherCode": "296",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0033_cloudy_with_light_rain_night.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Light rain"
                            }
                        ],
                        "precipMM": "1.9",
                        "humidity": "92",
                        "visibility": "8",
                        "pressure": "997",
                        "cloudcover": "76",
                        "HeatIndexC": "9",
                        "HeatIndexF": "49",
                        "DewPointC": "8",
                        "DewPointF": "47",
                        "WindChillC": "9",
                        "WindChillF": "48",
                        "WindGustMiles": "5",
                        "WindGustKmph": "8",
                        "FeelsLikeC": "9",
                        "FeelsLikeF": "48",
                        "chanceofrain": "96",
                        "chanceofremdry": "0",
                        "chanceofwindy": "0",
                        "chanceofovercast": "85",
                        "chanceofsunshine": "0",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    },
                    {
                        "time": "900",
                        "tempC": "11",
                        "tempF": "53",
                        "windspeedMiles": "7",
                        "windspeedKmph": "11",
                        "winddirDegree": "91",
                        "winddir16Point": "E",
                        "weatherCode": "122",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Overcast"
                            }
                        ],
                        "precipMM": "0.0",
                        "humidity": "83",
                        "visibility": "19",
                        "pressure": "998",
                        "cloudcover": "100",
                        "HeatIndexC": "11",
                        "HeatIndexF": "53",
                        "DewPointC": "9",
                        "DewPointF": "48",
                        "WindChillC": "10",
                        "WindChillF": "50",
                        "WindGustMiles": "8",
                        "WindGustKmph": "13",
                        "FeelsLikeC": "10",
                        "FeelsLikeF": "50",
                        "chanceofrain": "0",
                        "chanceofremdry": "93",
                        "chanceofwindy": "0",
                        "chanceofovercast": "90",
                        "chanceofsunshine": "11",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    },
                    {
                        "time": "1200",
                        "tempC": "13",
                        "tempF": "56",
                        "windspeedMiles": "11",
                        "windspeedKmph": "17",
                        "winddirDegree": "102",
                        "winddir16Point": "ESE",
                        "weatherCode": "176",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0009_light_rain_showers.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Patchy rain possible"
                            }
                        ],
                        "precipMM": "0.1",
                        "humidity": "76",
                        "visibility": "20",
                        "pressure": "1000",
                        "cloudcover": "100",
                        "HeatIndexC": "13",
                        "HeatIndexF": "56",
                        "DewPointC": "9",
                        "DewPointF": "49",
                        "WindChillC": "12",
                        "WindChillF": "53",
                        "WindGustMiles": "14",
                        "WindGustKmph": "22",
                        "FeelsLikeC": "12",
                        "FeelsLikeF": "53",
                        "chanceofrain": "80",
                        "chanceofremdry": "0",
                        "chanceofwindy": "0",
                        "chanceofovercast": "89",
                        "chanceofsunshine": "0",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    },
                    {
                        "time": "1500",
                        "tempC": "15",
                        "tempF": "60",
                        "windspeedMiles": "12",
                        "windspeedKmph": "19",
                        "winddirDegree": "121",
                        "winddir16Point": "ESE",
                        "weatherCode": "176",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0009_light_rain_showers.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Patchy rain possible"
                            }
                        ],
                        "precipMM": "0.5",
                        "humidity": "54",
                        "visibility": "17",
                        "pressure": "1001",
                        "cloudcover": "72",
                        "HeatIndexC": "15",
                        "HeatIndexF": "60",
                        "DewPointC": "6",
                        "DewPointF": "43",
                        "WindChillC": "15",
                        "WindChillF": "60",
                        "WindGustMiles": "15",
                        "WindGustKmph": "23",
                        "FeelsLikeC": "15",
                        "FeelsLikeF": "60",
                        "chanceofrain": "93",
                        "chanceofremdry": "0",
                        "chanceofwindy": "0",
                        "chanceofovercast": "88",
                        "chanceofsunshine": "0",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    },
                    {
                        "time": "1800",
                        "tempC": "15",
                        "tempF": "59",
                        "windspeedMiles": "7",
                        "windspeedKmph": "12",
                        "winddirDegree": "133",
                        "winddir16Point": "SE",
                        "weatherCode": "176",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0009_light_rain_showers.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Patchy rain possible"
                            }
                        ],
                        "precipMM": "0.5",
                        "humidity": "67",
                        "visibility": "17",
                        "pressure": "1003",
                        "cloudcover": "75",
                        "HeatIndexC": "15",
                        "HeatIndexF": "59",
                        "DewPointC": "9",
                        "DewPointF": "47",
                        "WindChillC": "14",
                        "WindChillF": "58",
                        "WindGustMiles": "9",
                        "WindGustKmph": "14",
                        "FeelsLikeC": "14",
                        "FeelsLikeF": "58",
                        "chanceofrain": "92",
                        "chanceofremdry": "0",
                        "chanceofwindy": "0",
                        "chanceofovercast": "88",
                        "chanceofsunshine": "0",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    },
                    {
                        "time": "2100",
                        "tempC": "12",
                        "tempF": "53",
                        "windspeedMiles": "6",
                        "windspeedKmph": "10",
                        "winddirDegree": "124",
                        "winddir16Point": "ESE",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "humidity": "82",
                        "visibility": "20",
                        "pressure": "1006",
                        "cloudcover": "14",
                        "HeatIndexC": "12",
                        "HeatIndexF": "53",
                        "DewPointC": "9",
                        "DewPointF": "48",
                        "WindChillC": "11",
                        "WindChillF": "51",
                        "WindGustMiles": "8",
                        "WindGustKmph": "13",
                        "FeelsLikeC": "11",
                        "FeelsLikeF": "51",
                        "chanceofrain": "0",
                        "chanceofremdry": "86",
                        "chanceofwindy": "0",
                        "chanceofovercast": "30",
                        "chanceofsunshine": "78",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    }
                ]
            },
            {
                "date": "2018-04-11",
                "astronomy": [
                    {
                        "sunrise": "06:14 AM",
                        "sunset": "07:51 PM",
                        "moonrise": "04:42 AM",
                        "moonset": "02:30 PM",
                        "moon_phase": "Waning Crescent",
                        "moon_illumination": "34"
                    }
                ],
                "maxtempC": "18",
                "maxtempF": "65",
                "mintempC": "9",
                "mintempF": "48",
                "totalSnow_cm": "0.0",
                "sunHour": "11.6",
                "uvIndex": "3",
                "hourly": [
                    {
                        "time": "0",
                        "tempC": "11",
                        "tempF": "52",
                        "windspeedMiles": "4",
                        "windspeedKmph": "6",
                        "winddirDegree": "99",
                        "winddir16Point": "E",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "humidity": "86",
                        "visibility": "19",
                        "pressure": "1007",
                        "cloudcover": "11",
                        "HeatIndexC": "11",
                        "HeatIndexF": "52",
                        "DewPointC": "9",
                        "DewPointF": "48",
                        "WindChillC": "10",
                        "WindChillF": "51",
                        "WindGustMiles": "5",
                        "WindGustKmph": "8",
                        "FeelsLikeC": "10",
                        "FeelsLikeF": "51",
                        "chanceofrain": "0",
                        "chanceofremdry": "87",
                        "chanceofwindy": "0",
                        "chanceofovercast": "36",
                        "chanceofsunshine": "74",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    },
                    {
                        "time": "300",
                        "tempC": "10",
                        "tempF": "50",
                        "windspeedMiles": "3",
                        "windspeedKmph": "5",
                        "winddirDegree": "60",
                        "winddir16Point": "ENE",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "humidity": "89",
                        "visibility": "20",
                        "pressure": "1007",
                        "cloudcover": "18",
                        "HeatIndexC": "10",
                        "HeatIndexF": "50",
                        "DewPointC": "8",
                        "DewPointF": "47",
                        "WindChillC": "9",
                        "WindChillF": "49",
                        "WindGustMiles": "5",
                        "WindGustKmph": "8",
                        "FeelsLikeC": "9",
                        "FeelsLikeF": "49",
                        "chanceofrain": "0",
                        "chanceofremdry": "90",
                        "chanceofwindy": "0",
                        "chanceofovercast": "32",
                        "chanceofsunshine": "71",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    },
                    {
                        "time": "600",
                        "tempC": "9",
                        "tempF": "49",
                        "windspeedMiles": "5",
                        "windspeedKmph": "8",
                        "winddirDegree": "47",
                        "winddir16Point": "NE",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "humidity": "90",
                        "visibility": "15",
                        "pressure": "1008",
                        "cloudcover": "26",
                        "HeatIndexC": "9",
                        "HeatIndexF": "49",
                        "DewPointC": "8",
                        "DewPointF": "46",
                        "WindChillC": "8",
                        "WindChillF": "47",
                        "WindGustMiles": "8",
                        "WindGustKmph": "12",
                        "FeelsLikeC": "8",
                        "FeelsLikeF": "47",
                        "chanceofrain": "0",
                        "chanceofremdry": "82",
                        "chanceofwindy": "0",
                        "chanceofovercast": "41",
                        "chanceofsunshine": "70",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    },
                    {
                        "time": "900",
                        "tempC": "14",
                        "tempF": "57",
                        "windspeedMiles": "6",
                        "windspeedKmph": "10",
                        "winddirDegree": "76",
                        "winddir16Point": "ENE",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "humidity": "70",
                        "visibility": "8",
                        "pressure": "1008",
                        "cloudcover": "18",
                        "HeatIndexC": "14",
                        "HeatIndexF": "57",
                        "DewPointC": "8",
                        "DewPointF": "47",
                        "WindChillC": "13",
                        "WindChillF": "55",
                        "WindGustMiles": "7",
                        "WindGustKmph": "12",
                        "FeelsLikeC": "13",
                        "FeelsLikeF": "55",
                        "chanceofrain": "0",
                        "chanceofremdry": "87",
                        "chanceofwindy": "0",
                        "chanceofovercast": "48",
                        "chanceofsunshine": "77",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    },
                    {
                        "time": "1200",
                        "tempC": "18",
                        "tempF": "64",
                        "windspeedMiles": "7",
                        "windspeedKmph": "11",
                        "winddirDegree": "94",
                        "winddir16Point": "E",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "humidity": "51",
                        "visibility": "11",
                        "pressure": "1008",
                        "cloudcover": "12",
                        "HeatIndexC": "18",
                        "HeatIndexF": "64",
                        "DewPointC": "8",
                        "DewPointF": "46",
                        "WindChillC": "18",
                        "WindChillF": "64",
                        "WindGustMiles": "8",
                        "WindGustKmph": "13",
                        "FeelsLikeC": "18",
                        "FeelsLikeF": "64",
                        "chanceofrain": "0",
                        "chanceofremdry": "80",
                        "chanceofwindy": "0",
                        "chanceofovercast": "35",
                        "chanceofsunshine": "79",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    },
                    {
                        "time": "1500",
                        "tempC": "18",
                        "tempF": "65",
                        "windspeedMiles": "7",
                        "windspeedKmph": "12",
                        "winddirDegree": "95",
                        "winddir16Point": "E",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "humidity": "48",
                        "visibility": "14",
                        "pressure": "1006",
                        "cloudcover": "24",
                        "HeatIndexC": "18",
                        "HeatIndexF": "65",
                        "DewPointC": "7",
                        "DewPointF": "45",
                        "WindChillC": "18",
                        "WindChillF": "65",
                        "WindGustMiles": "8",
                        "WindGustKmph": "13",
                        "FeelsLikeC": "18",
                        "FeelsLikeF": "65",
                        "chanceofrain": "0",
                        "chanceofremdry": "81",
                        "chanceofwindy": "0",
                        "chanceofovercast": "30",
                        "chanceofsunshine": "82",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    },
                    {
                        "time": "1800",
                        "tempC": "16",
                        "tempF": "61",
                        "windspeedMiles": "9",
                        "windspeedKmph": "14",
                        "winddirDegree": "73",
                        "winddir16Point": "ENE",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "humidity": "58",
                        "visibility": "18",
                        "pressure": "1006",
                        "cloudcover": "6",
                        "HeatIndexC": "16",
                        "HeatIndexF": "61",
                        "DewPointC": "8",
                        "DewPointF": "46",
                        "WindChillC": "16",
                        "WindChillF": "61",
                        "WindGustMiles": "10",
                        "WindGustKmph": "16",
                        "FeelsLikeC": "16",
                        "FeelsLikeF": "61",
                        "chanceofrain": "0",
                        "chanceofremdry": "86",
                        "chanceofwindy": "0",
                        "chanceofovercast": "32",
                        "chanceofsunshine": "76",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    },
                    {
                        "time": "2100",
                        "tempC": "13",
                        "tempF": "55",
                        "windspeedMiles": "8",
                        "windspeedKmph": "13",
                        "winddirDegree": "68",
                        "winddir16Point": "ENE",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "humidity": "68",
                        "visibility": "15",
                        "pressure": "1007",
                        "cloudcover": "6",
                        "HeatIndexC": "13",
                        "HeatIndexF": "55",
                        "DewPointC": "7",
                        "DewPointF": "45",
                        "WindChillC": "12",
                        "WindChillF": "53",
                        "WindGustMiles": "10",
                        "WindGustKmph": "15",
                        "FeelsLikeC": "12",
                        "FeelsLikeF": "53",
                        "chanceofrain": "0",
                        "chanceofremdry": "88",
                        "chanceofwindy": "0",
                        "chanceofovercast": "45",
                        "chanceofsunshine": "72",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    }
                ]
            },
            {
                "date": "2018-04-12",
                "astronomy": [
                    {
                        "sunrise": "06:11 AM",
                        "sunset": "07:52 PM",
                        "moonrise": "05:09 AM",
                        "moonset": "03:37 PM",
                        "moon_phase": "Waning Crescent",
                        "moon_illumination": "28"
                    }
                ],
                "maxtempC": "13",
                "maxtempF": "55",
                "mintempC": "6",
                "mintempF": "43",
                "totalSnow_cm": "0.0",
                "sunHour": "6.7",
                "uvIndex": "1",
                "hourly": [
                    {
                        "time": "0",
                        "tempC": "11",
                        "tempF": "51",
                        "windspeedMiles": "7",
                        "windspeedKmph": "12",
                        "winddirDegree": "42",
                        "winddir16Point": "NE",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "humidity": "77",
                        "visibility": "15",
                        "pressure": "1006",
                        "cloudcover": "14",
                        "HeatIndexC": "11",
                        "HeatIndexF": "51",
                        "DewPointC": "7",
                        "DewPointF": "44",
                        "WindChillC": "9",
                        "WindChillF": "49",
                        "WindGustMiles": "9",
                        "WindGustKmph": "15",
                        "FeelsLikeC": "9",
                        "FeelsLikeF": "49",
                        "chanceofrain": "0",
                        "chanceofremdry": "90",
                        "chanceofwindy": "0",
                        "chanceofovercast": "34",
                        "chanceofsunshine": "78",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    },
                    {
                        "time": "300",
                        "tempC": "9",
                        "tempF": "49",
                        "windspeedMiles": "8",
                        "windspeedKmph": "13",
                        "winddirDegree": "19",
                        "winddir16Point": "NNE",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "humidity": "86",
                        "visibility": "14",
                        "pressure": "1004",
                        "cloudcover": "25",
                        "HeatIndexC": "9",
                        "HeatIndexF": "49",
                        "DewPointC": "7",
                        "DewPointF": "45",
                        "WindChillC": "7",
                        "WindChillF": "45",
                        "WindGustMiles": "12",
                        "WindGustKmph": "19",
                        "FeelsLikeC": "7",
                        "FeelsLikeF": "45",
                        "chanceofrain": "0",
                        "chanceofremdry": "93",
                        "chanceofwindy": "0",
                        "chanceofovercast": "40",
                        "chanceofsunshine": "88",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    },
                    {
                        "time": "600",
                        "tempC": "9",
                        "tempF": "48",
                        "windspeedMiles": "13",
                        "windspeedKmph": "22",
                        "winddirDegree": "23",
                        "winddir16Point": "NNE",
                        "weatherCode": "122",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Overcast"
                            }
                        ],
                        "precipMM": "0.0",
                        "humidity": "90",
                        "visibility": "20",
                        "pressure": "1003",
                        "cloudcover": "100",
                        "HeatIndexC": "9",
                        "HeatIndexF": "48",
                        "DewPointC": "7",
                        "DewPointF": "45",
                        "WindChillC": "6",
                        "WindChillF": "42",
                        "WindGustMiles": "18",
                        "WindGustKmph": "30",
                        "FeelsLikeC": "6",
                        "FeelsLikeF": "42",
                        "chanceofrain": "0",
                        "chanceofremdry": "91",
                        "chanceofwindy": "0",
                        "chanceofovercast": "94",
                        "chanceofsunshine": "9",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    },
                    {
                        "time": "900",
                        "tempC": "11",
                        "tempF": "53",
                        "windspeedMiles": "11",
                        "windspeedKmph": "17",
                        "winddirDegree": "35",
                        "winddir16Point": "NE",
                        "weatherCode": "266",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0017_cloudy_with_light_rain.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Light drizzle"
                            }
                        ],
                        "precipMM": "0.2",
                        "humidity": "90",
                        "visibility": "20",
                        "pressure": "1002",
                        "cloudcover": "100",
                        "HeatIndexC": "11",
                        "HeatIndexF": "53",
                        "DewPointC": "10",
                        "DewPointF": "50",
                        "WindChillC": "9",
                        "WindChillF": "49",
                        "WindGustMiles": "16",
                        "WindGustKmph": "26",
                        "FeelsLikeC": "9",
                        "FeelsLikeF": "49",
                        "chanceofrain": "82",
                        "chanceofremdry": "0",
                        "chanceofwindy": "0",
                        "chanceofovercast": "92",
                        "chanceofsunshine": "0",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    },
                    {
                        "time": "1200",
                        "tempC": "13",
                        "tempF": "55",
                        "windspeedMiles": "11",
                        "windspeedKmph": "17",
                        "winddirDegree": "41",
                        "winddir16Point": "NE",
                        "weatherCode": "266",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0017_cloudy_with_light_rain.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Light drizzle"
                            }
                        ],
                        "precipMM": "0.3",
                        "humidity": "86",
                        "visibility": "20",
                        "pressure": "1001",
                        "cloudcover": "100",
                        "HeatIndexC": "13",
                        "HeatIndexF": "55",
                        "DewPointC": "10",
                        "DewPointF": "50",
                        "WindChillC": "11",
                        "WindChillF": "51",
                        "WindGustMiles": "15",
                        "WindGustKmph": "25",
                        "FeelsLikeC": "11",
                        "FeelsLikeF": "51",
                        "chanceofrain": "75",
                        "chanceofremdry": "0",
                        "chanceofwindy": "0",
                        "chanceofovercast": "81",
                        "chanceofsunshine": "0",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    },
                    {
                        "time": "1500",
                        "tempC": "13",
                        "tempF": "55",
                        "windspeedMiles": "10",
                        "windspeedKmph": "15",
                        "winddirDegree": "60",
                        "winddir16Point": "ENE",
                        "weatherCode": "266",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0017_cloudy_with_light_rain.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Light drizzle"
                            }
                        ],
                        "precipMM": "0.3",
                        "humidity": "86",
                        "visibility": "20",
                        "pressure": "1000",
                        "cloudcover": "100",
                        "HeatIndexC": "13",
                        "HeatIndexF": "55",
                        "DewPointC": "10",
                        "DewPointF": "50",
                        "WindChillC": "11",
                        "WindChillF": "52",
                        "WindGustMiles": "13",
                        "WindGustKmph": "21",
                        "FeelsLikeC": "11",
                        "FeelsLikeF": "52",
                        "chanceofrain": "65",
                        "chanceofremdry": "0",
                        "chanceofwindy": "0",
                        "chanceofovercast": "89",
                        "chanceofsunshine": "0",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "38"
                    },
                    {
                        "time": "1800",
                        "tempC": "12",
                        "tempF": "53",
                        "windspeedMiles": "9",
                        "windspeedKmph": "14",
                        "winddirDegree": "105",
                        "winddir16Point": "ESE",
                        "weatherCode": "389",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0024_thunderstorms.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Moderate or heavy rain with thunder"
                            }
                        ],
                        "precipMM": "1.5",
                        "humidity": "90",
                        "visibility": "20",
                        "pressure": "1001",
                        "cloudcover": "100",
                        "HeatIndexC": "12",
                        "HeatIndexF": "53",
                        "DewPointC": "10",
                        "DewPointF": "51",
                        "WindChillC": "10",
                        "WindChillF": "51",
                        "WindGustMiles": "12",
                        "WindGustKmph": "19",
                        "FeelsLikeC": "10",
                        "FeelsLikeF": "51",
                        "chanceofrain": "68",
                        "chanceofremdry": "0",
                        "chanceofwindy": "0",
                        "chanceofovercast": "94",
                        "chanceofsunshine": "0",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "32"
                    },
                    {
                        "time": "2100",
                        "tempC": "6",
                        "tempF": "43",
                        "windspeedMiles": "4",
                        "windspeedKmph": "6",
                        "winddirDegree": "113",
                        "winddir16Point": "ESE",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "humidity": "82",
                        "visibility": "18",
                        "pressure": "1002",
                        "cloudcover": "2",
                        "HeatIndexC": "9",
                        "HeatIndexF": "48",
                        "DewPointC": "11",
                        "DewPointF": "53",
                        "WindChillC": "6",
                        "WindChillF": "43",
                        "WindGustMiles": "5",
                        "WindGustKmph": "8",
                        "FeelsLikeC": "6",
                        "FeelsLikeF": "43",
                        "chanceofrain": "0",
                        "chanceofremdry": "80",
                        "chanceofwindy": "0",
                        "chanceofovercast": "37",
                        "chanceofsunshine": "82",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    }
                ]
            },
            {
                "date": "2018-04-13",
                "astronomy": [
                    {
                        "sunrise": "06:09 AM",
                        "sunset": "07:54 PM",
                        "moonrise": "05:34 AM",
                        "moonset": "04:46 PM",
                        "moon_phase": "Waning Crescent",
                        "moon_illumination": "21"
                    }
                ],
                "maxtempC": "18",
                "maxtempF": "64",
                "mintempC": "11",
                "mintempF": "52",
                "totalSnow_cm": "0.0",
                "sunHour": "9.2",
                "uvIndex": "4",
                "hourly": [
                    {
                        "time": "0",
                        "tempC": "11",
                        "tempF": "51",
                        "windspeedMiles": "7",
                        "windspeedKmph": "12",
                        "winddirDegree": "89",
                        "winddir16Point": "E",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "humidity": "77",
                        "visibility": "14",
                        "pressure": "1003",
                        "cloudcover": "7",
                        "HeatIndexC": "11",
                        "HeatIndexF": "51",
                        "DewPointC": "7",
                        "DewPointF": "44",
                        "WindChillC": "9",
                        "WindChillF": "49",
                        "WindGustMiles": "11",
                        "WindGustKmph": "17",
                        "FeelsLikeC": "9",
                        "FeelsLikeF": "49",
                        "chanceofrain": "0",
                        "chanceofremdry": "85",
                        "chanceofwindy": "0",
                        "chanceofovercast": "37",
                        "chanceofsunshine": "85",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    },
                    {
                        "time": "300",
                        "tempC": "10",
                        "tempF": "50",
                        "windspeedMiles": "7",
                        "windspeedKmph": "12",
                        "winddirDegree": "106",
                        "winddir16Point": "ESE",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "humidity": "81",
                        "visibility": "8",
                        "pressure": "1004",
                        "cloudcover": "15",
                        "HeatIndexC": "10",
                        "HeatIndexF": "50",
                        "DewPointC": "7",
                        "DewPointF": "45",
                        "WindChillC": "8",
                        "WindChillF": "47",
                        "WindGustMiles": "12",
                        "WindGustKmph": "19",
                        "FeelsLikeC": "8",
                        "FeelsLikeF": "47",
                        "chanceofrain": "0",
                        "chanceofremdry": "80",
                        "chanceofwindy": "0",
                        "chanceofovercast": "42",
                        "chanceofsunshine": "78",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    },
                    {
                        "time": "600",
                        "tempC": "10",
                        "tempF": "50",
                        "windspeedMiles": "7",
                        "windspeedKmph": "12",
                        "winddirDegree": "119",
                        "winddir16Point": "ESE",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "humidity": "78",
                        "visibility": "9",
                        "pressure": "1005",
                        "cloudcover": "17",
                        "HeatIndexC": "10",
                        "HeatIndexF": "50",
                        "DewPointC": "6",
                        "DewPointF": "43",
                        "WindChillC": "8",
                        "WindChillF": "47",
                        "WindGustMiles": "11",
                        "WindGustKmph": "18",
                        "FeelsLikeC": "8",
                        "FeelsLikeF": "47",
                        "chanceofrain": "0",
                        "chanceofremdry": "81",
                        "chanceofwindy": "0",
                        "chanceofovercast": "30",
                        "chanceofsunshine": "80",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    },
                    {
                        "time": "900",
                        "tempC": "14",
                        "tempF": "58",
                        "windspeedMiles": "10",
                        "windspeedKmph": "16",
                        "winddirDegree": "130",
                        "winddir16Point": "SE",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "humidity": "56",
                        "visibility": "20",
                        "pressure": "1006",
                        "cloudcover": "14",
                        "HeatIndexC": "14",
                        "HeatIndexF": "58",
                        "DewPointC": "6",
                        "DewPointF": "42",
                        "WindChillC": "13",
                        "WindChillF": "55",
                        "WindGustMiles": "11",
                        "WindGustKmph": "18",
                        "FeelsLikeC": "13",
                        "FeelsLikeF": "55",
                        "chanceofrain": "0",
                        "chanceofremdry": "89",
                        "chanceofwindy": "0",
                        "chanceofovercast": "49",
                        "chanceofsunshine": "87",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    },
                    {
                        "time": "1200",
                        "tempC": "18",
                        "tempF": "64",
                        "windspeedMiles": "13",
                        "windspeedKmph": "21",
                        "winddirDegree": "126",
                        "winddir16Point": "SE",
                        "weatherCode": "119",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0003_white_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "humidity": "41",
                        "visibility": "20",
                        "pressure": "1006",
                        "cloudcover": "69",
                        "HeatIndexC": "18",
                        "HeatIndexF": "64",
                        "DewPointC": "4",
                        "DewPointF": "40",
                        "WindChillC": "18",
                        "WindChillF": "64",
                        "WindGustMiles": "15",
                        "WindGustKmph": "24",
                        "FeelsLikeC": "18",
                        "FeelsLikeF": "64",
                        "chanceofrain": "0",
                        "chanceofremdry": "81",
                        "chanceofwindy": "0",
                        "chanceofovercast": "93",
                        "chanceofsunshine": "13",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    },
                    {
                        "time": "1500",
                        "tempC": "18",
                        "tempF": "64",
                        "windspeedMiles": "14",
                        "windspeedKmph": "23",
                        "winddirDegree": "120",
                        "winddir16Point": "ESE",
                        "weatherCode": "176",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0009_light_rain_showers.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Patchy rain possible"
                            }
                        ],
                        "precipMM": "0.5",
                        "humidity": "47",
                        "visibility": "20",
                        "pressure": "1006",
                        "cloudcover": "87",
                        "HeatIndexC": "18",
                        "HeatIndexF": "64",
                        "DewPointC": "6",
                        "DewPointF": "43",
                        "WindChillC": "18",
                        "WindChillF": "64",
                        "WindGustMiles": "17",
                        "WindGustKmph": "28",
                        "FeelsLikeC": "18",
                        "FeelsLikeF": "64",
                        "chanceofrain": "89",
                        "chanceofremdry": "0",
                        "chanceofwindy": "0",
                        "chanceofovercast": "81",
                        "chanceofsunshine": "0",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    },
                    {
                        "time": "1800",
                        "tempC": "16",
                        "tempF": "60",
                        "windspeedMiles": "13",
                        "windspeedKmph": "21",
                        "winddirDegree": "106",
                        "winddir16Point": "ESE",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "humidity": "56",
                        "visibility": "20",
                        "pressure": "1007",
                        "cloudcover": "17",
                        "HeatIndexC": "16",
                        "HeatIndexF": "60",
                        "DewPointC": "7",
                        "DewPointF": "44",
                        "WindChillC": "16",
                        "WindChillF": "60",
                        "WindGustMiles": "18",
                        "WindGustKmph": "29",
                        "FeelsLikeC": "16",
                        "FeelsLikeF": "60",
                        "chanceofrain": "0",
                        "chanceofremdry": "89",
                        "chanceofwindy": "0",
                        "chanceofovercast": "34",
                        "chanceofsunshine": "71",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    },
                    {
                        "time": "2100",
                        "tempC": "13",
                        "tempF": "56",
                        "windspeedMiles": "10",
                        "windspeedKmph": "16",
                        "winddirDegree": "107",
                        "winddir16Point": "ESE",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "humidity": "64",
                        "visibility": "17",
                        "pressure": "1008",
                        "cloudcover": "36",
                        "HeatIndexC": "13",
                        "HeatIndexF": "56",
                        "DewPointC": "7",
                        "DewPointF": "44",
                        "WindChillC": "12",
                        "WindChillF": "53",
                        "WindGustMiles": "15",
                        "WindGustKmph": "24",
                        "FeelsLikeC": "12",
                        "FeelsLikeF": "53",
                        "chanceofrain": "0",
                        "chanceofremdry": "81",
                        "chanceofwindy": "0",
                        "chanceofovercast": "45",
                        "chanceofsunshine": "88",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    }
                ]
            },
            {
                "date": "2018-04-14",
                "astronomy": [
                    {
                        "sunrise": "06:07 AM",
                        "sunset": "07:56 PM",
                        "moonrise": "05:57 AM",
                        "moonset": "05:58 PM",
                        "moon_phase": "Waning Crescent",
                        "moon_illumination": "14"
                    }
                ],
                "maxtempC": "21",
                "maxtempF": "69",
                "mintempC": "13",
                "mintempF": "55",
                "totalSnow_cm": "0.0",
                "sunHour": "11.6",
                "uvIndex": "3",
                "hourly": [
                    {
                        "time": "0",
                        "tempC": "12",
                        "tempF": "54",
                        "windspeedMiles": "6",
                        "windspeedKmph": "10",
                        "winddirDegree": "108",
                        "winddir16Point": "ESE",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "humidity": "71",
                        "visibility": "13",
                        "pressure": "1009",
                        "cloudcover": "28",
                        "HeatIndexC": "12",
                        "HeatIndexF": "54",
                        "DewPointC": "7",
                        "DewPointF": "45",
                        "WindChillC": "11",
                        "WindChillF": "53",
                        "WindGustMiles": "9",
                        "WindGustKmph": "15",
                        "FeelsLikeC": "11",
                        "FeelsLikeF": "53",
                        "chanceofrain": "0",
                        "chanceofremdry": "81",
                        "chanceofwindy": "0",
                        "chanceofovercast": "34",
                        "chanceofsunshine": "80",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    },
                    {
                        "time": "300",
                        "tempC": "12",
                        "tempF": "53",
                        "windspeedMiles": "5",
                        "windspeedKmph": "8",
                        "winddirDegree": "137",
                        "winddir16Point": "SE",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "humidity": "80",
                        "visibility": "20",
                        "pressure": "1009",
                        "cloudcover": "13",
                        "HeatIndexC": "12",
                        "HeatIndexF": "53",
                        "DewPointC": "8",
                        "DewPointF": "47",
                        "WindChillC": "11",
                        "WindChillF": "52",
                        "WindGustMiles": "7",
                        "WindGustKmph": "11",
                        "FeelsLikeC": "11",
                        "FeelsLikeF": "52",
                        "chanceofrain": "0",
                        "chanceofremdry": "85",
                        "chanceofwindy": "0",
                        "chanceofovercast": "41",
                        "chanceofsunshine": "77",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    },
                    {
                        "time": "600",
                        "tempC": "11",
                        "tempF": "52",
                        "windspeedMiles": "4",
                        "windspeedKmph": "7",
                        "winddirDegree": "154",
                        "winddir16Point": "SSE",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "humidity": "85",
                        "visibility": "19",
                        "pressure": "1010",
                        "cloudcover": "21",
                        "HeatIndexC": "11",
                        "HeatIndexF": "52",
                        "DewPointC": "9",
                        "DewPointF": "47",
                        "WindChillC": "10",
                        "WindChillF": "51",
                        "WindGustMiles": "7",
                        "WindGustKmph": "11",
                        "FeelsLikeC": "10",
                        "FeelsLikeF": "51",
                        "chanceofrain": "0",
                        "chanceofremdry": "89",
                        "chanceofwindy": "0",
                        "chanceofovercast": "38",
                        "chanceofsunshine": "85",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    },
                    {
                        "time": "900",
                        "tempC": "16",
                        "tempF": "60",
                        "windspeedMiles": "6",
                        "windspeedKmph": "9",
                        "winddirDegree": "167",
                        "winddir16Point": "SSE",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "humidity": "67",
                        "visibility": "20",
                        "pressure": "1011",
                        "cloudcover": "14",
                        "HeatIndexC": "16",
                        "HeatIndexF": "60",
                        "DewPointC": "10",
                        "DewPointF": "49",
                        "WindChillC": "16",
                        "WindChillF": "60",
                        "WindGustMiles": "7",
                        "WindGustKmph": "11",
                        "FeelsLikeC": "16",
                        "FeelsLikeF": "60",
                        "chanceofrain": "0",
                        "chanceofremdry": "90",
                        "chanceofwindy": "0",
                        "chanceofovercast": "35",
                        "chanceofsunshine": "77",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    },
                    {
                        "time": "1200",
                        "tempC": "20",
                        "tempF": "68",
                        "windspeedMiles": "6",
                        "windspeedKmph": "10",
                        "winddirDegree": "172",
                        "winddir16Point": "S",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "humidity": "49",
                        "visibility": "20",
                        "pressure": "1012",
                        "cloudcover": "17",
                        "HeatIndexC": "20",
                        "HeatIndexF": "68",
                        "DewPointC": "9",
                        "DewPointF": "48",
                        "WindChillC": "20",
                        "WindChillF": "68",
                        "WindGustMiles": "7",
                        "WindGustKmph": "12",
                        "FeelsLikeC": "20",
                        "FeelsLikeF": "68",
                        "chanceofrain": "0",
                        "chanceofremdry": "84",
                        "chanceofwindy": "0",
                        "chanceofovercast": "42",
                        "chanceofsunshine": "82",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    },
                    {
                        "time": "1500",
                        "tempC": "21",
                        "tempF": "69",
                        "windspeedMiles": "7",
                        "windspeedKmph": "11",
                        "winddirDegree": "177",
                        "winddir16Point": "S",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "humidity": "47",
                        "visibility": "20",
                        "pressure": "1011",
                        "cloudcover": "16",
                        "HeatIndexC": "21",
                        "HeatIndexF": "69",
                        "DewPointC": "9",
                        "DewPointF": "48",
                        "WindChillC": "21",
                        "WindChillF": "69",
                        "WindGustMiles": "8",
                        "WindGustKmph": "13",
                        "FeelsLikeC": "21",
                        "FeelsLikeF": "69",
                        "chanceofrain": "0",
                        "chanceofremdry": "80",
                        "chanceofwindy": "0",
                        "chanceofovercast": "46",
                        "chanceofsunshine": "79",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    },
                    {
                        "time": "1800",
                        "tempC": "19",
                        "tempF": "66",
                        "windspeedMiles": "6",
                        "windspeedKmph": "10",
                        "winddirDegree": "160",
                        "winddir16Point": "SSE",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "humidity": "54",
                        "visibility": "20",
                        "pressure": "1012",
                        "cloudcover": "27",
                        "HeatIndexC": "19",
                        "HeatIndexF": "66",
                        "DewPointC": "9",
                        "DewPointF": "49",
                        "WindChillC": "19",
                        "WindChillF": "66",
                        "WindGustMiles": "7",
                        "WindGustKmph": "12",
                        "FeelsLikeC": "19",
                        "FeelsLikeF": "66",
                        "chanceofrain": "0",
                        "chanceofremdry": "85",
                        "chanceofwindy": "0",
                        "chanceofovercast": "32",
                        "chanceofsunshine": "89",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    },
                    {
                        "time": "2100",
                        "tempC": "16",
                        "tempF": "62",
                        "windspeedMiles": "4",
                        "windspeedKmph": "7",
                        "winddirDegree": "97",
                        "winddir16Point": "E",
                        "weatherCode": "116",
                        "weatherIconUrl": [
                            {
                                "value": "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png"
                            }
                        ],
                        "weatherDesc": [
                            {
                                "value": "Partly cloudy"
                            }
                        ],
                        "precipMM": "0.0",
                        "humidity": "68",
                        "visibility": "20",
                        "pressure": "1013",
                        "cloudcover": "15",
                        "HeatIndexC": "16",
                        "HeatIndexF": "62",
                        "DewPointC": "10",
                        "DewPointF": "51",
                        "WindChillC": "16",
                        "WindChillF": "62",
                        "WindGustMiles": "6",
                        "WindGustKmph": "9",
                        "FeelsLikeC": "16",
                        "FeelsLikeF": "62",
                        "chanceofrain": "0",
                        "chanceofremdry": "86",
                        "chanceofwindy": "0",
                        "chanceofovercast": "44",
                        "chanceofsunshine": "86",
                        "chanceoffrost": "0",
                        "chanceofhightemp": "0",
                        "chanceoffog": "0",
                        "chanceofsnow": "0",
                        "chanceofthunder": "0"
                    }
                ]
            }
        ],
        "ClimateAverages": [
            {
                "month": [
                    {
                        "index": "1",
                        "name": "January",
                        "avgMinTemp": "2.4",
                        "avgMinTemp_F": "36.3",
                        "absMaxTemp": "8.6",
                        "absMaxTemp_F": "47.5",
                        "avgDailyRainfall": "2.44"
                    },
                    {
                        "index": "2",
                        "name": "February",
                        "avgMinTemp": "2.4",
                        "avgMinTemp_F": "36.3",
                        "absMaxTemp": "9.7",
                        "absMaxTemp_F": "49.5",
                        "avgDailyRainfall": "2.32"
                    },
                    {
                        "index": "3",
                        "name": "March",
                        "avgMinTemp": "3.7",
                        "avgMinTemp_F": "38.7",
                        "absMaxTemp": "14.7",
                        "absMaxTemp_F": "58.5",
                        "avgDailyRainfall": "1.74"
                    },
                    {
                        "index": "4",
                        "name": "April",
                        "avgMinTemp": "5.9",
                        "avgMinTemp_F": "42.6",
                        "absMaxTemp": "18.7",
                        "absMaxTemp_F": "65.7",
                        "avgDailyRainfall": "1.74"
                    },
                    {
                        "index": "5",
                        "name": "May",
                        "avgMinTemp": "9.6",
                        "avgMinTemp_F": "49.3",
                        "absMaxTemp": "18.8",
                        "absMaxTemp_F": "65.8",
                        "avgDailyRainfall": "2.17"
                    },
                    {
                        "index": "6",
                        "name": "June",
                        "avgMinTemp": "12.7",
                        "avgMinTemp_F": "54.9",
                        "absMaxTemp": "22.7",
                        "absMaxTemp_F": "72.9",
                        "avgDailyRainfall": "2.41"
                    },
                    {
                        "index": "7",
                        "name": "July",
                        "avgMinTemp": "14.5",
                        "avgMinTemp_F": "58.1",
                        "absMaxTemp": "26.1",
                        "absMaxTemp_F": "79.0",
                        "avgDailyRainfall": "2.43"
                    },
                    {
                        "index": "8",
                        "name": "August",
                        "avgMinTemp": "14.0",
                        "avgMinTemp_F": "57.2",
                        "absMaxTemp": "24.6",
                        "absMaxTemp_F": "76.3",
                        "avgDailyRainfall": "2.26"
                    },
                    {
                        "index": "9",
                        "name": "September",
                        "avgMinTemp": "11.6",
                        "avgMinTemp_F": "52.9",
                        "absMaxTemp": "22.9",
                        "absMaxTemp_F": "73.2",
                        "avgDailyRainfall": "1.41"
                    },
                    {
                        "index": "10",
                        "name": "October",
                        "avgMinTemp": "9.6",
                        "avgMinTemp_F": "49.3",
                        "absMaxTemp": "17.5",
                        "absMaxTemp_F": "63.5",
                        "avgDailyRainfall": "1.82"
                    },
                    {
                        "index": "11",
                        "name": "November",
                        "avgMinTemp": "6.1",
                        "avgMinTemp_F": "43.0",
                        "absMaxTemp": "12.8",
                        "absMaxTemp_F": "55.0",
                        "avgDailyRainfall": "2.31"
                    },
                    {
                        "index": "12",
                        "name": "December",
                        "avgMinTemp": "3.7",
                        "avgMinTemp_F": "38.7",
                        "absMaxTemp": "12.1",
                        "absMaxTemp_F": "53.8",
                        "avgDailyRainfall": "2.68"
                    }
                ]
            }
        ]
    }
}

result = response.data.current_condition[0].temp_C;
console.log(result);
