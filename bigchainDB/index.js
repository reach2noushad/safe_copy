const BigchainDB = require('bigchaindb-driver')

const API_PATH = 'https://test.bigchaindb.com/api/v1/'
const conn = new BigchainDB.Connection(API_PATH)

const bip39 = require('bip39')



const seed = bip39.mnemonicToSeed('seedPhrase').slice(0,32)

const alice = new BigchainDB.Ed25519Keypair(seed)


const DID = require('./did');



const car = new BigchainDB.Ed25519Keypair()
const sensorGPS = new BigchainDB.Ed25519Keypair()

const userDID = new DID(alice.publicKey)
// console.log(userDID);
const carDID = new DID(car.publicKey)
const gpsDID = new DID(sensorGPS.publicKey)

userDID.define("myModel", "https://schema.org/v1/myModel")
// console.log("second",userDID);
carDID.define("myModel", "https://schema.org/v1/myModel")
gpsDID.define("myModel", "https://schema.org/v1/myModel")

userDID.models.myModel.create({
    keypair: alice,
    data: {
        name: 'Alice',
        bithday: '03/08/1910'
    }
}).then(asset => {
    userDID.id = 'did:' + asset.id
    document.body.innerHTML +='<h3>Transaction created</h3>'
    document.body.innerHTML +=asset.id
})

// console.log(userDID);

let mycarDID;

const vehicle = {
    value: '6sd8f68sd67',
    power: {
      engine: '2.5',
      hp: '220 hp',
    },
    consumption: '10.8 l'
  }

  carDID.models.myModel.create({
    keypair: alice,
    data: {
        vehicle
    }
}).then(asset => {
    carDID.id = 'did:' + asset.id;
    document.body.innerHTML +='<h3>Transaction created</h3>'
    document.body.innerHTML +=txTelemetrySigned.id
})

gpsDID.models.myModel.create({
    keypair: car,
    data: {
        gps_identifier: 'a32bc2440da012'
    }
}).then(asset => {
    gpsDID.id =  'did:' + asset.id
    document.body.innerHTML +='<h3>Transaction created</h3>'
    document.body.innerHTML +=txTelemetrySigned.id

})

function updateMileage(did, newMileage){
    console.log("reaching mileage function");
    did.models.myModel
    .retrieve(did.id)
    .then(assets => {
        console.log("this is assets",assets);
        // assets is an array of myModel
        // the retrieve asset contains the last (unspent) state
        // of the asset
        return did,assets[0].append({
            toPublicKey: car.publicKey,
            keypair: car,
            data: { newMileage }
        });
    })
    // .then(updatedAsset => {
    //     console.log("reaching",updatedAsset);
    //     did.mileage =  updatedAsset.data.newMileage
    //     document.body.innerHTML +='<h3>Append transaction created</h3>'
    //     document.body.innerHTML +=txTelemetrySigned.id
    //     console.log(updatedAsset);
    //     return updatedAsset
    // })
}

// console.log(carDID);

// console.log(JSON.stringify(carDID));

function newUpdateAsset(updatedAsset) {
    console.log("reaching update asset function",updatedAsset);
        did.mileage =  updatedAsset.data.newMileage
        document.body.innerHTML +='<h3>Append transaction created</h3>'
        document.body.innerHTML +=txTelemetrySigned.id
        console.log(updatedAsset);
        return updatedAsset
}



const sleep = m => new Promise(r => setTimeout(r, m));

async function myfunction() {
    // await sleep(3000);
    const result = await updateMileage(carDID,1200);
    console.log(result);
    // console.log("finished");
    
    // const newResult = await newUpdateAsset(result);
    // await sleep(6000);
    // console.log(newResult);
}

await sleep(7000);

myfunction();

