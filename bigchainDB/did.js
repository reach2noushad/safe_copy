const Orm = require('bigchaindb-orm/src/index.js')
const API_PATH = 'https://test.bigchaindb.com/api/v1/';

class DID extends Orm {
    constructor(entity) {
        super(API_PATH)
        this.entity = entity
    }
}

module.exports = DID;