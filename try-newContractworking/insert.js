var ethers = require('ethers');
var provider = ethers.providers.getDefaultProvider('rinkeby');
var Web3 = require('web3');
const web3 = new Web3();

var abi=[{"constant":false,"inputs":[{"name":"instowner","type":"address"},{"name":"hash","type":"string"}],"name":"addhashpatient","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"patientowner","type":"address"},{"name":"p","type":"uint256"},{"name":"d","type":"uint256"},{"name":"secondowner","type":"address"}],"name":"cancelsecondowner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"instowner","type":"address"},{"name":"adhaar","type":"bytes32"},{"name":"inputUUID","type":"bytes32"},{"name":"comb","type":"string"},{"name":"department","type":"bytes32"},{"name":"hospital","type":"bytes32"}],"name":"insertDoctor","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"instowner","type":"address"},{"name":"adhaar","type":"bytes32"},{"name":"inputUUID","type":"bytes32"},{"name":"comb","type":"string"}],"name":"insertPatient","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"patientaddress","type":"address"},{"name":"doctoraddress","type":"address"}],"name":"secondOwner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"doctor","type":"address"}],"name":"getGivenAccesslist","outputs":[{"name":"","type":"address[]"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"instowner","type":"address"},{"name":"UUIDs","type":"bytes32"},{"name":"index","type":"uint256"}],"name":"gethashpatient","outputs":[{"name":"hash","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"Doctor","type":"address"},{"name":"d","type":"uint256"},{"name":"pindex","type":"uint256"}],"name":"getPatient","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"Doctor","type":"address"},{"name":"d","type":"uint256"}],"name":"getpatienthashdoctor","outputs":[{"name":"hash","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"patient","type":"address"}],"name":"getPatientSharedList","outputs":[{"name":"","type":"address[]"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"instowner","type":"address"},{"name":"UUIDs","type":"bytes32"},{"name":"aadhar","type":"bytes32"},{"name":"index","type":"uint256"}],"name":"loginDoctor","outputs":[{"name":"","type":"string"},{"name":"","type":"bytes32"},{"name":"","type":"bytes32"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"instowner","type":"address"},{"name":"UUIDs","type":"bytes32"},{"name":"aadhar","type":"bytes32"},{"name":"index","type":"uint256"}],"name":"loginpatient","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"}];


var address = '0x3740051cb536a8d21e3bfb1ca4a84518e73ceae8';

var privateKey = '0xb6cc8c319ed7df803b864d895fdd3deb67437ffc5d2377ed90bdaddc132a61d4';

var wallet = new ethers.Wallet(privateKey, provider);

var contract = new ethers.Contract(address, abi, wallet);

// var myaddress = "0xF3DacA5383245AcedF64963B271dfF6dC116F883";
// var aadhaar = 151515151515;
// var newUuid = "a36459d5929a1b124f90a195a3d";
// var binout = "first string inout identifier";


var myaddress = "0xF3DacA5383245AcedF64963B271dfF6dC116F883";
var aadhaar = 151515151515;
var newUuid = "b345679d5929a1b124f90a195a3d";
var binout = "second string inout identifier";

var sendPromise = contract.insertDoctor(myaddress,web3.utils.fromAscii(aadhaar),web3.utils.fromAscii(newUuid),binout,web3.utils.fromAscii("oncology"),web3.utils.fromAscii("mims"));

sendPromise.then(function(transaction) {
    console.log(transaction);
});


