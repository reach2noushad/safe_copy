 var ethers = require('ethers');
 var provider = ethers.providers.getDefaultProvider('rinkeby');
 var Web3 = require('web3');
 const web3 = new Web3();

 // web3.setProvider(new Web3.providers.HttpProvider('http://35.185.16.215:8545'))

 web3.setProvider(new Web3.providers.HttpProvider('https://rinkeby.infura.io/vsiKIQUoLZGiUvoxa2Az'))



 var abi=[{"constant":false,"inputs":[{"name":"instowner","type":"address"},{"name":"hash","type":"string"}],"name":"addhashpatient","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"firstowner","type":"address"},{"name":"secondowner","type":"address"}],"name":"cancelsecondowner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"instowner","type":"address"},{"name":"adhaar","type":"bytes32"},{"name":"UUID","type":"bytes32"},{"name":"comb","type":"bytes32"}],"name":"insertDoctor","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"instowner","type":"address"},{"name":"UUIDs","type":"bytes32"},{"name":"aadhar","type":"bytes32"}],"name":"loginpatient","outputs":[{"name":"","type":"bytes32"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"instowner","type":"address"},{"name":"UUIDs","type":"bytes32"}],"name":"gethashpatient","outputs":[{"name":"hash","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"presentowner","type":"address"},{"name":"secondowner","type":"address"}],"name":"secondOwner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"Doctor","type":"address"}],"name":"getPatient","outputs":[{"name":"","type":"bytes32"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"instowner","type":"address"},{"name":"adhaar","type":"bytes32"},{"name":"UUID","type":"bytes32"},{"name":"comb","type":"bytes32"}],"name":"insertPatient","outputs":[{"name":"","type":"bytes32"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"Doctor","type":"address"}],"name":"getpatienthashdoctor","outputs":[{"name":"hash","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"instowner","type":"address"},{"name":"UUIDs","type":"bytes32"},{"name":"aadhar","type":"bytes32"}],"name":"loginDoctor","outputs":[{"name":"","type":"bytes32"}],"payable":false,"stateMutability":"view","type":"function"}];

// console.log(abi);

 var address = '0xaefe42c95da8bf091451dc5e03bd8c2c4cbae25a';

 var privateKey = '0xb6cc8c319ed7df803b864d895fdd3deb67437ffc5d2377ed90bdaddc132a61d4';

 var wallet = new ethers.Wallet(privateKey, provider);

 var contract = new ethers.Contract(address, abi, wallet);

 var myaddress = "0xF3DacA5383245AcedF64963B271dfF6dC116F883";


// var testFoo = "395df8f7c51f007019cb30201c49e884b46b92fa";
// console.log(typeof(testFoo));
// var testFooObj = new String(testFoo);
// console.log(testFooObj.valueOf());

// console.log(ethers.utils);

function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

var uuid = guid();
var newStr = uuid.replace(/-/g, '');
console.log(newStr);





var str = "03087ecab42e71dac0c276d121f6427b0a69b512";
var binout = str.slice(0, 32);
console.log(binout);

 var sendPromise = contract.insertDoctor(myaddress,web3.utils.fromAscii("12345678"),web3.utils.fromAscii(newStr),web3.utils.fromAscii(binout));


 // var sendPromise = contract.insertDoctor(myaddress,web3.utils.fromAscii("12345678"),web3.utils.fromAscii("a614dc09-d463-4bf7-837d-250047be3dbe"),web3.utils.fromAscii("1234"));

 sendPromise.then(function(transaction) {
     console.log(transaction);
 });


 // myContract = new web3.eth.Contract((abi),'0x5fa2a2b272ccfdb81657af7a3e0c50c88bb39e56');
 //
 // // console.log(myContract.options);
 //
 // // myContract.insertDoctor.sendTransaction(myaddress,web3.utils.fromAscii("12345678"),web3.utils.fromAscii("a614dc09-d463-4bf7-837d-250047be3dbe"),testFoo,web3.utils.fromAscii("mims"),web3.utils.fromAscii("hospital"),{from: "0xF3DacA5383245AcedF64963B271dfF6dC116F883"}).then(function(transaction){
 // //   console.log(transaction);
 // console.log(myContract.methods);

//  myContract.methods.insertDoctor(myaddress,web3.utils.fromAscii("12345678"),web3.utils.fromAscii("250047be"),'395df8f7c51f007019cb30201c49e884b46b92fa',web3.utils.fromAscii("mims"),web3.utils.fromAscii("hospital")).send({from: '0xF3DacA5383245AcedF64963B271dfF6dC116F883'})
// .then(function(receipt){
//   console.log("starting");
//   console.log(receipt);
//   console.log("End");
//     // receipt can also be a new contract instance, when coming from a "contract.deploy({...}).send()"
// });

// myContract.methods.loginDoctor("0xF3DacA5383245AcedF64963B271dfF6dC116F883",web3.utils.fromAscii("250047be3dbe"),web3.utils.fromAscii("12345678")).call({from: '0xF3DacA5383245AcedF64963B271dfF6dC116F883'})
// .then(function(result){
//     console.log(result);
//     });




// myContract.methods.loginDoctor("0xF3DacA5383245AcedF64963B271dfF6dC116F883",web3.utils.fromAscii("0x12314564"),web3.utils.fromAscii("12345678")).call()
// .then(console.log);

 // myContract.insertDoctor(myaddress,web3.utils.fromAscii("12345678"),web3.utils.fromAscii("a614dc09-d463-4bf7-837d-250047be3dbe"),testFoo,web3.utils.fromAscii("mims"),web3.utils.fromAscii("hospital"),{from:"0xF3DacA5383245AcedF64963B271dfF6dC116F883"}, function(error, result){
 //     if(!error) {
 //         console.log("#" + result + "#")
 //     } else {
 //         console.error(error);
 //     }
 // })
