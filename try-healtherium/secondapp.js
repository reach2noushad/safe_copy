var ethers = require('ethers');
var provider = ethers.providers.getDefaultProvider('rinkeby');
var Web3 = require('web3');
const web3 = new Web3();

web3.setProvider(new Web3.providers.HttpProvider('http://35.185.16.215:8545'))

 var abi=[{"constant":false,"inputs":[{"name":"instowner","type":"address"},{"name":"hash","type":"string"}],"name":"addhashpatient","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"firstowner","type":"address"},{"name":"secondowner","type":"address"}],"name":"cancelsecondowner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"instowner","type":"address"},{"name":"adhaar","type":"bytes32"},{"name":"UUID","type":"bytes32"},{"name":"comb","type":"bytes32"}],"name":"insertDoctor","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"instowner","type":"address"},{"name":"UUIDs","type":"bytes32"},{"name":"aadhar","type":"bytes32"}],"name":"loginpatient","outputs":[{"name":"","type":"bytes32"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"instowner","type":"address"},{"name":"UUIDs","type":"bytes32"}],"name":"gethashpatient","outputs":[{"name":"hash","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"presentowner","type":"address"},{"name":"secondowner","type":"address"}],"name":"secondOwner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"Doctor","type":"address"}],"name":"getPatient","outputs":[{"name":"","type":"bytes32"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"instowner","type":"address"},{"name":"adhaar","type":"bytes32"},{"name":"UUID","type":"bytes32"},{"name":"comb","type":"bytes32"}],"name":"insertPatient","outputs":[{"name":"","type":"bytes32"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"Doctor","type":"address"}],"name":"getpatienthashdoctor","outputs":[{"name":"hash","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"instowner","type":"address"},{"name":"UUIDs","type":"bytes32"},{"name":"aadhar","type":"bytes32"}],"name":"loginDoctor","outputs":[{"name":"","type":"bytes32"}],"payable":false,"stateMutability":"view","type":"function"}];



var address = '0xaefe42c95da8bf091451dc5e03bd8c2c4cbae25a';



var contract = new ethers.Contract(address, abi, provider);

var callPromise = contract.loginDoctor("0xF3DacA5383245AcedF64963B271dfF6dC116F883",web3.utils.fromAscii("1e8ab66f4c477e4569f5cebd7b437c26"),web3.utils.fromAscii("12345678"));



// var callPromise = contract.loginDoctor("0xF3DacA5383245AcedF64963B271dfF6dC116F883",web3.utils.fromAscii("a614dc09-d463-4bf7-837d-250047be3dbe"),web3.utils.fromAscii("12345678"));
// console.log(callPromise);
callPromise.then(function(result){
  console.log(result);
  console.log(web3.utils.toAscii(result));
});
//
// console.log(ethers.utils);



//


// callPromise.then(function(value) {
//     console.log('Single Return Value:' + value);
// });

// var privateKey = '0xb6cc8c319ed7df803b864d895fdd3deb67437ffc5d2377ed90bdaddc132a61d4';
//
// var wallet = new ethers.Wallet(privateKey, provider);
//
// var contract = new ethers.Contract(address, abi, wallet);
//
// var sendPromise = contract.insertDoctor("0xF3DacA5383245AcedF64963B271dfF6dC116F883",web3.utils.fromAscii("12345678"),web3.utils.fromAscii("a614dc09-d463-4bf7-837d-250047be3dbe"),web3.utils.fromAscii("395df8f7c51f007019cb30201c49e884b46b92fa"),web3.utils.fromAscii("mims"),web3.utils.fromAscii("hospital"));
//
// sendPromise.then(function(transaction) {
//     console.log(transaction);
// });
