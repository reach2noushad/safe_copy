
var ethers = require('ethers');
var providers = require('ethers').providers;
var provider = providers.getDefaultProvider('rinkeby');

var address = '0x8aa2ed4d2bf29cd53b5cc723f66fa150d558446c';

var abi = [{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"getValue","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"n","type":"string"}],"name":"setValue","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"nonpayable","type":"function"}];

var contract = new ethers.Contract(address,abi,provider);

var callPromise = contract.getValue();

callPromise.then(function(result){
  console.log(result);
})
