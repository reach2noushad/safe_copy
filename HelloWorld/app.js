var ethers = require('ethers');
var providers = require('ethers').providers;
var provider = providers.getDefaultProvider('rinkeby');

var address = '0x8aa2ed4d2bf29cd53b5cc723f66fa150d558446c';

var abi = [{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"getValue","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"n","type":"string"}],"name":"setValue","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"nonpayable","type":"function"}];

var privateKey = '0x3ab6468f2465130c51946a5456b8e2d309be7af2f8afcd6823996d281c0990d0';

var wallet = new ethers.Wallet(privateKey,provider);

var contract = new ethers.Contract(address,abi,wallet);

var sendPromise = contract.setValue("Zalma Thatha");

sendPromise.then(function(transaction){
  console.log(transaction);
  console.log(transaction.hash);

})
