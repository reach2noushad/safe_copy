pragma solidity ^0.4.18;

contract HelloWorld {
    
    string public name;
    
    function setValue(string n) returns (string){
        name = n;
        return name;
    }
    
    function getValue() public constant returns (string){
        return name;
    }
    
}
