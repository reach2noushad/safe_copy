
//Transfer 2 Ethers
// var ethers = require('ethers');
// var privateKey = '0x7dcd3fcd70f6df14b46749ae0fd491048d9c0743c3e20f59bb0363554a02caaf';
// var wallet = new ethers.Wallet(privateKey);
// wallet.provider = ethers.providers.getDefaultProvider('rinkeby');
// var amount = ethers.utils.parseEther('2.0');
// var address = '0xa3632b6979345e6fe2184890f7e965f6a3fed1c2';
// var sendPromise = wallet.send(address, amount);
//
// sendPromise.then(function(transactionHash) {
//     console.log(transactionHash);
// });


//Checking Balance

var ethers = require('ethers');
var providers = ethers.providers;
var provider = providers.getDefaultProvider('rinkeby');
var address = '0xa3632b6979345e6fe2184890f7e965f6a3fed1c2';
provider.getBalance(address).then(function(balance) {

    // balance is a BigNumber (in wei); format is as a sting (in ether)
    var etherString = ethers.utils.formatEther(balance);

    console.log("Balance: " + etherString);
});
