var keythereum = require("keythereum");

var params = { keyBytes: 32, ivBytes: 16 };
var dk = keythereum.create(params);
// var password = "wheethereum";
var password = "noushad";
var kdf = "pbkdf2"; // or "scrypt" to use the scrypt kdf
var options = {
  kdf: "pbkdf2",
  cipher: "aes-128-ctr",
  kdfparams: {
    c: 262144,
    dklen: 32,
    prf: "hmac-sha256"
  }
};

var keyObject = keythereum.dump(password, dk.privateKey, dk.salt, dk.iv, options);

console.log(keyObject.address);

keythereum.exportToFile(keyObject,'/home/mohamednoushad/webtorm/10_trylightnode/keystore');

var privateKey = keythereum.recover(password, keyObject);

console.log(privateKey.toString('hex'));
