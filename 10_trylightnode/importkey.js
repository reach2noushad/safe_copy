var keythereum = require("keythereum");
const FS = require('fs');
const PATH = require('path');

function readFiles(dirPath, processFile) {
  FS.readdir(dirPath, (err, fileNames) => {
    if (err) throw err;
    fileNames.forEach((fileName) => {
      let name = PATH.parse(fileName).name;
      let ext = PATH.parse(fileName).ext;
      let path = PATH.resolve(dirPath, fileName);
      processFile(name, ext, path);
    });
  });
}


var datadir = "/home/mohamednoushad/webtorm/10_trylightnode";

readFiles(datadir+'/keystore', (name, ext, path) => {
  var mypath = path;
  console.log(mypath);
  FS.readFile(mypath, (err, data) => {
    if (err) throw err;
    console.log(data);
    var myfile = JSON.parse(data);
    var address = myfile.address;
    console.log(address);
    var keyObject = keythereum.importFromFile(address, datadir);
    // var password = "wheethereum";
    var password = "noushad";
    var privateKey = keythereum.recover(password, keyObject);
    console.log(privateKey.toString('hex'));
  });
});
