var crypto = require('asymmetric-crypto');
// import * as crypto from 'asymmetric-crypto'

// Generate a key pair
// const keyPair = crypto.keyPair()
// console.log(keyPair);
// -> {
//   secretKey: 'KOy7fMWMkRc+QX8dzpfX9VwJKlc/+Zkyw5C7RGTXT920IjiKUdOSe/3sNnrETw7ej9TBFzsPyRfkWGMsGLAufQ==',
//   publicKey: 'tCI4ilHTknv97DZ6xE8O3o/UwRc7D8kX5FhjLBiwLn0='
// }

// Regenerate a key pair from the secret key
// const newKeyPair = crypto.fromSecretKey(keyPair.secretKey)
// console.log(newKeyPair);
// -> {
//   secretKey: 'KOy7fMWMkRc+QX8dzpfX9VwJKlc/+Zkyw5C7RGTXT920IjiKUdOSe/3sNnrETw7ej9TBFzsPyRfkWGMsGLAufQ==',
//   publicKey: 'tCI4ilHTknv97DZ6xE8O3o/UwRc7D8kX5FhjLBiwLn0='
// }

const myKeyPair = crypto.keyPair()
console.log(myKeyPair);
const theirKeyPair = crypto.keyPair()
console.log(theirKeyPair);

// Encrypt data
const encrypted = crypto.encrypt('some data', theirKeyPair.publicKey, myKeyPair.secretKey)
console.log(encrypted);
// -> {
//   data: '63tP2r8WQuJ+k+jzsd8pbT6WYPHMTafpeg==',
//   nonce: 'BDHALdoeBiGg7wJbVdfJhVQQyvpxrBSo'
// }

// Decrypt data
const decrypted = crypto.decrypt(encrypted.data, encrypted.nonce, myKeyPair.publicKey, theirKeyPair.secretKey)
console.log(decrypted);
// -> 'some data'

// Sign a message
const message = 'some message'
const signature = crypto.sign(message, myKeyPair.secretKey)
// -> '8oz1aNkSBG1qvYhc+E2VBkgHSxCORGdsyf7LFQuLDmZvJt6vaEzHMIsofmTykMunhCrChEHT9Fgw3sp/W6+7Bw=='

// Verify the signature on a message
const validSignature = crypto.verify(message, signature, myKeyPair.publicKey)
// -> true
