var crypto = require('asymmetric-crypto');


var myKeyPair = { secretKey: new Uint8Array('3ab6468f2465130c51946a5456b8e2d309be7af2f8afcd6823996d281c0990d0'),
  publicKey: new Uint8Array('7c477A59578710eC7bfD2bf29D7a24F53A33979a') };


var theirKeyPair = { secretKey: new Uint8Array('a6092ae235f58c2da0cf95834b37bca6038897e5240a5699244ec00f3dc16d43'),
  publicKey: new Uint8Array('319743Be1A6c1Dc638841CE48b43D36118f8BcD2') };


const encrypted = crypto.encrypt('da39a3ee5e6b4b0d3255bfef95601890afd80709', theirKeyPair.publicKey, myKeyPair.secretKey)
console.log(encrypted);

// var encrypted = { data: 'Zhzd8a28nEYruxm9sIsvLE+/bHna7ikaQ6Z3Br+2+Q3JhJqP4PfkzRmamnJufvWpHx89LX+yvKk=',
//   nonce: 'tv7TlzhCjZj4CZr9vmV1CiGIcjC1lHNV' }


const decrypted = crypto.decrypt(encrypted.data, encrypted.nonce, myKeyPair.publicKey, theirKeyPair.secretKey)
console.log(decrypted);
