var ethers = require('ethers');
const web3 = require('web3');


var abi = [{"constant":false,"inputs":[{"name":"instowner","type":"address"},{"name":"hash","type":"string"}],"name":"addhashpatient","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"firstowner","type":"address"},{"name":"secondowner","type":"address"}],"name":"cancelsecondowner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"instowner","type":"address"},{"name":"adhaar","type":"bytes32"},{"name":"UUID","type":"bytes32"},{"name":"comb","type":"string"},{"name":"department","type":"bytes32"},{"name":"hospital","type":"bytes32"}],"name":"insertDoctor","outputs":[{"name":"","type":"string"},{"name":"","type":"string"},{"name":"","type":"string"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"instowner","type":"address"},{"name":"adhaar","type":"bytes32"},{"name":"UUID","type":"bytes32"},{"name":"comb","type":"string"}],"name":"insertPatient","outputs":[{"name":"","type":"string"},{"name":"","type":"string"},{"name":"","type":"string"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"presentowner","type":"address"},{"name":"secondowner","type":"address"}],"name":"secondOwner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"instowner","type":"address"},{"name":"UUIDs","type":"bytes32"}],"name":"gethashpatient","outputs":[{"name":"hash","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"Doctor","type":"address"}],"name":"getPatient","outputs":[{"name":"","type":"string"},{"name":"","type":"string"},{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"Doctor","type":"address"}],"name":"getpatienthashdoctor","outputs":[{"name":"hash","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"instowner","type":"address"},{"name":"UUIDs","type":"bytes32"},{"name":"aadhar","type":"bytes32"}],"name":"loginDoctor","outputs":[{"name":"","type":"string"},{"name":"","type":"string"},{"name":"","type":"string"},{"name":"","type":"bytes32"},{"name":"","type":"bytes32"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"instowner","type":"address"},{"name":"UUIDs","type":"bytes32"},{"name":"aadhar","type":"bytes32"}],"name":"loginpatient","outputs":[{"name":"","type":"string"},{"name":"","type":"string"},{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"}];

var address = '0xb717fdc117f25c5ca60190b9a94c2336df01e532';
var provider = ethers.providers.getDefaultProvider('rinkeby');


var contract = new ethers.Contract(address,abi,provider);

var inputuuid = '05ba7515663a4e95a521c572af68342f';

var callPromise = contract.loginDoctor('0xe45508c1899def662d443bacf77e06e4d4a99ff6',web3.utils.fromAscii(inputuuid),web3.utils.fromAscii("12345678"));

callPromise.then(function(result){
  console.log(result);
  console.log(web3.utils.toUtf8(result[3]));
  console.log(web3.utils.toUtf8(result[4]));
}).catch(function () {
     console.log("Promise Rejected");
});
