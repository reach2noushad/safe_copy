 var ethers = require('ethers');

//  var abi = [{"constant": false,"inputs": [{"name": "value","type": "string"}],"name": "setValue","outputs": [],"payable": false,"stateMutability": "nonpayable","type": "function"},{"constant": true,"inputs": [],"name": "getAuthorAndValue","outputs":[{"name": "author","type": "address"},{"name": "value","type": "string"}],"payable": false,"stateMutability": "view","type": "function"},{"constant": true,"inputs": [],"name": "getValue","outputs": [{"name": "value","type": "string"}],"payable":false,"stateMutability": "view","type": "function"}];
//
var address = '0x874fe1b5a8c59919b735855e2f4b57b6e497622e';

var provider = ethers.providers.getDefaultProvider('rinkeby');

var contract = new ethers.Contract(address, abi, provider);

var callPromise = contract.getValue();
console.log(callPromise);
//
// callPromise.then(function(value) {
//     console.log('Single Return Value:' + value);
// });

// var privateKey = '0xb6cc8c319ed7df803b864d895fdd3deb67437ffc5d2377ed90bdaddc132a61d4';
//
// var wallet = new ethers.Wallet(privateKey, provider);
//
// var contract = new ethers.Contract(address, abi, wallet);
//
// var sendPromise = contract.setValue("Mohamed Noushad");
//
// sendPromise.then(function(transaction) {
//     console.log(transaction);
// });
