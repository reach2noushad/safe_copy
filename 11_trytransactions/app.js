const uuidv4 = require('uuid/v4');
var ethers = require('ethers');
const web3 = require('web3');



var abi = [{"constant":false,"inputs":[{"name":"instowner","type":"address"},{"name":"hash","type":"string"}],"name":"addhashpatient","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"firstowner","type":"address"},{"name":"secondowner","type":"address"}],"name":"cancelsecondowner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"instowner","type":"address"},{"name":"adhaar","type":"bytes32"},{"name":"UUID","type":"bytes32"},{"name":"comb","type":"string"},{"name":"department","type":"bytes32"},{"name":"hospital","type":"bytes32"}],"name":"insertDoctor","outputs":[{"name":"","type":"string"},{"name":"","type":"string"},{"name":"","type":"string"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"instowner","type":"address"},{"name":"adhaar","type":"bytes32"},{"name":"UUID","type":"bytes32"},{"name":"comb","type":"string"}],"name":"insertPatient","outputs":[{"name":"","type":"string"},{"name":"","type":"string"},{"name":"","type":"string"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"presentowner","type":"address"},{"name":"secondowner","type":"address"}],"name":"secondOwner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"instowner","type":"address"},{"name":"UUIDs","type":"bytes32"}],"name":"gethashpatient","outputs":[{"name":"hash","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"Doctor","type":"address"}],"name":"getPatient","outputs":[{"name":"","type":"string"},{"name":"","type":"string"},{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"Doctor","type":"address"}],"name":"getpatienthashdoctor","outputs":[{"name":"hash","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"instowner","type":"address"},{"name":"UUIDs","type":"bytes32"},{"name":"aadhar","type":"bytes32"}],"name":"loginDoctor","outputs":[{"name":"","type":"string"},{"name":"","type":"string"},{"name":"","type":"string"},{"name":"","type":"bytes32"},{"name":"","type":"bytes32"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"instowner","type":"address"},{"name":"UUIDs","type":"bytes32"},{"name":"aadhar","type":"bytes32"}],"name":"loginpatient","outputs":[{"name":"","type":"string"},{"name":"","type":"string"},{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"}];

var address = '0xb717fdc117f25c5ca60190b9a94c2336df01e532';
var provider = ethers.providers.getDefaultProvider('rinkeby');
// var privateKey = '0xb6cc8c319ed7df803b864d895fdd3deb67437ffc5d2377ed90bdaddc132a61d4';
var privateKey = '0x48a7aea873ff33d93dbcfd18bf49e69a9866e4bcfe1dcd9fe1b1a9c00696149f';
var wallet = new ethers.Wallet(privateKey, provider);

var contract = new ethers.Contract(address,abi,wallet);

var newStr = '05ba7515663a4e95a521c572af68342f';

// var blockinout = 'da39a3ee5e6b4b0d3255bfef95601890afd80709';

var blockinout =  '7ce8f2ba49d5159a6999259cf20cc0bf19d0511a';

var sendPromise = contract.insertDoctor('0xe45508c1899def662d443bacf77e06e4d4a99ff6',web3.utils.fromAscii('12345678'),web3.utils.fromAscii(newStr),blockinout,web3.utils.fromAscii("Oncology"),web3.utils.fromAscii("MIMS"));

sendPromise.then(function(transaction){
  console.log(transaction);
  console.log(transaction.hash);
  provider.waitForTransaction(transaction.hash).then(function(transaction){
    console.log("Transaction Mined "+transaction.hash);
    console.log(transaction);
  });
}).catch(function () {
     console.log("Promise Rejected");
});
