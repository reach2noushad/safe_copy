var ethers = require('ethers');

var provider = ethers.providers.getDefaultProvider('');
var address  = '0x82214A01AEd08EDB889116Aed8816fBbe22D505f';
var abi =
[{"constant":false,"inputs":[{"name":"_fName","type":"string"},{"name":"_age","type":"uint256"}],"name":"setInstructor","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"getInstructor","outputs":[{"name":"","type":"string"},{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"anonymous":false,"inputs":[{"indexed":false,"name":"name","type":"string"},{"indexed":false,"name":"age","type":"uint256"}],"name":"Instructor","type":"event"}];

var contract = new ethers.Contract(address,abi,provider);

var callPromise = contract.getInstructor();

callPromise.then(function(result){
  console.log(result);
});
