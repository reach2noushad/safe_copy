var ethers = require('ethers');
var utils = require('ethers').utils;
var Interface = ethers.Interface;
var abi =
[{"constant":false,"inputs":[{"name":"_fName","type":"string"},{"name":"_age","type":"uint256"}],"name":"setInstructor","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"getInstructor","outputs":[{"name":"","type":"string"},{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"anonymous":false,"inputs":[{"indexed":false,"name":"name","type":"string"},{"indexed":false,"name":"age","type":"uint256"}],"name":"Instructor","type":"event"}];
var iface = new Interface(abi);
var transferInfo = iface.events.Instructor;
var provider = ethers.providers.getDefaultProvider('rinkeby');


provider.on(transferInfo.topics, function(log) {

    result = transferInfo.parse(log.topics, log.data);
    console.log('all entries: ', result);
    console.log(result.name);
    console.log(result.age.toNumber());


});
